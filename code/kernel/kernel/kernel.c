#include <stdio.h>
#include <bmath.h>
#include <string.h>

#include <kernel/tty.h>


void kernel_main(void)
{
    int x;
    char number[20];

    terminal_initialize();
    printf("mOS v0.1 alpha\n");

    for (x = 0; x < 80; ++x)
        putchar('-');

    printf("Hello, World!\n");
    
    for (x = 0; x < 1000; ++x)
    {
        itoa(x, number);
        printf(number);
        putchar(' ');
    }
}
