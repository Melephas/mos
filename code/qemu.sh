#!/bin/sh

cd /home/sam/code/mOS/code/

set -e
. ./iso.sh

qemu-system-$(./target-triplet-to-arch.sh $HOST) -cdrom myos.iso
