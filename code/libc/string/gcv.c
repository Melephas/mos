#include <string.h>
#include <bmath.h>

char gcv(int number, int column)
{
    int result = number / exp(10, column);
    result = result % 10;
    return (char) result + '0';
}