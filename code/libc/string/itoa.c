#include <string.h>
#include <bmath.h>

void itoa(int number, char *string)
{
    if (number == 0)
    {
        string[0] = '0';
        string[1] = '\0';
        return;
    }
    int i = 0, x;
    while (exp(10, i) <= number)
        ++i;

    for (x = 0; x < i; ++x)
        string[x] = gcv(number, i - (x + 1));
    string[i] = '\0';
    return;
}
