#include <bmath.h>

int exp(int base, int exponent)
{
    int result = base, i;
    if (exponent == 0)
        return 1;
    else if (exponent == 1)
        return base;
    else if (exponent > 1)
    {
        for (i = 1; i < exponent; ++i)
            result *= base;

        return result;
    }
}