#define HEAP_SIZE (8 * 1024 * 1024)
static uint8_t g_heap[HEAP_SIZE];


//http://man7.org/linux/man-pages/man2/brk.2.html

/*
        The breakline is used to break the heap into two parts: allocated and free.
        Everything before the breakline to the start of the buffer is considered 'allocated'.  (i.e. ptr < breakline)
        Everything between the end of the heap buffer and the breakline is 'free'.             (i.e. ptr >= breakline)
        Naturally the breakline starts of by pointing at the start of the heap, which means all memory is free.
 
        DO NOT ADJUST `g_breakline` -- use the breakline_*() functions to do it.
        The g_ is just there to remind you it's global when using it within functions
 */
static uint8_t *g_breakline = g_heap;

/*
        Sets the breakline to an explicit value in the heap.
 
        This is equivilent to Unix's brk
        In case of error the real brk would change the `errno` global variable to ENOMEM. We don't.
       
        return values:
          On error:
            -1
          On successs:
             0
 */
int breakline_set(uint8_t *new_waterline) {
    if (new_waterline < g_heap) {
        fprintf(stderr, "heap underflow\n");
        return -1;
    } else if (new_waterline >= (g_heap + HEAP_SIZE)) {
        fprintf(stderr, "heap overflow\n");
        return -1;
    }

    g_breakline = new_waterline;
    return 0;
}

/*
        Adjusts the breakline by an amount of bytes.
        Therefore a negative value for num_bytes 'frees' memorys, a positive one 'allocates' it.
       
        Calling breakline_adjust() with an increment of 0 can be used to find the current
        location of the program break.
       
        This is equivilent to Unix's sbrk -- don't think the s in sbrk means set ;)
        In case of error the real sbrk would change the `errno` global variable to ENOMEM. We don't.
       
        return values:
         On error:
             (void *) -1 is returned
         On successs:
             returns the previous program break.
             If the break was increased, then this value is a pointer to the start of the newly allocated memory
 */
void *breakline_adjust(ptrdiff_t num_bytes) {
    uint8_t *old_breakline = g_breakline;
    int error = breakline_set(old_breakline + num_bytes);
    if (error) {
        return (void *) - 1;
    } else {
        /* Return old_breakline rather than g_breakline as breakline_set will have changed g_breakline */
        return old_breakline;
    }
}