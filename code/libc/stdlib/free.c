#include <malloc.h>

void free(void *user_ptr) {
    uint8_t *current_breakline;
    struct malloc_secret_info *ptr_malloc_info;
    void *raw_ptr;
    uint8_t *original_breakline;
    size_t alloc_size;


    if (user_ptr == 0)
        return;


    /*
            from malloc():
                    user_ptr == raw_ptr + sizeof(struct malloc_secret_info)
            therefore:
                    user_ptr - sizeof(struct malloc_secret_info) == raw_ptr
 
            We recover the secret info so that we know how large the allocation is.
     */
    raw_ptr = ((uint8_t *) user_ptr) - sizeof (struct malloc_secret_info);
    ptr_malloc_info = (struct malloc_secret_info*) raw_ptr;


    if (ptr_malloc_info->raw_ptr != raw_ptr) {
        printf("double free or heap corruption?!\n");
        return;
    }

    alloc_size = sizeof (struct malloc_secret_info) +ptr_malloc_info->num_bytes;
    current_breakline = breakline_adjust(0);
    original_breakline = current_breakline - alloc_size;

    if (original_breakline == raw_ptr) {
        /*
                This pointer is the last thing allocated in the heap,
                therefore we can safely free the allocation by simply
                reducing the breakline!
         */
        breakline_adjust(-alloc_size);
    } else {
        /*
                We can't move the breakline as this allocation isn't at the end of it.
                Therefore the heap is now fragmented, as we don't record this "hole" in the heap and
                subsequent frees(), even if they are at the end of the heap, won't be able to skip past this hole.
 
                Fragmentation mangement is many, many, many lines of code and therefore not shown here.
                It's also one of the reasons malloc/free is to considered slow.
         */
    }

    /* destroy the malloc secret info so we can detect double frees etc */
    ptr_malloc_info->num_bytes = 0;
    ptr_malloc_info->raw_ptr = NULL;
    ptr_malloc_info->user_ptr = NULL;
}