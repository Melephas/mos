#include <malloc.h>

void* malloc(size_t num_bytes) {
    size_t alloc_size;
    void *raw_ptr;
    /*
            We over-allocate memory here so that we can fit in our own structure which will be used by free.
 
            We could put our secret structure either before or after the memory space we give to the user, but
            it's best to put it before, as buffer overflows are more common than underflows and therefore it's less
            likely that our data will be corrupted by a user error.
            (Though the user overflowing one allocation might affect others...)
 
            The caller of malloc will get back the memory seen from [user_ptr, user_ptr + num_bytes),
                    which is the correct size that they asked for.
     */

    if (num_bytes == 0)
        return 0;

    alloc_size = num_bytes + sizeof (struct malloc_secret_info);
    raw_ptr = breakline_adjust(alloc_size);
    if (raw_ptr == (void *) - 1) {
        return 0;
    } else {
        struct malloc_secret_info *ptr_malloc_info;
        uint8_t *user_ptr;

        ptr_malloc_info = (struct malloc_secret_info*) raw_ptr;
        user_ptr = ((uint8_t *) raw_ptr) + sizeof (struct malloc_secret_info);

        ptr_malloc_info->num_bytes = num_bytes;
        ptr_malloc_info->user_ptr = user_ptr;
        ptr_malloc_info->raw_ptr = raw_ptr;

        return user_ptr;
    }
}
