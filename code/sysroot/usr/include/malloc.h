/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   malloc.h
 * Author: sam
 *
 * Created on 18 July 2018, 18:10
 */

#ifndef MALLOC_H
#define MALLOC_H

#ifdef __cplusplus
extern "C" {
#endif

    struct malloc_secret_info {
        void* raw_ptr;
        uint8_t *user_ptr;
        size_t num_bytes;
    };

    int breakline_set(uint8_t*);
    void *breakline_adjust(ptrdiff_t);

#ifdef __cplusplus
}
#endif

#endif /* MALLOC_H */

