/* Generated automatically. */
static const char configuration_arguments[] = "../gcc-8.1.0/configure --target=i686-elf --prefix=/home/sam/code/mOS/buildchain --disable-nls --enable-languages=c,c++ --without-headers";
static const char thread_model[] = "single";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "generic" }, { "arch", "pentiumpro" } };
